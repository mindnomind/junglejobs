export const changeDate = (date) => {
  return {
    type: 'CHANGE_DATE',
    date
  }
}

export const setLabelFilter = (label) => {
  return {
    type: 'SET_LABEL_FILTER',
    label
  }
}

export const showAll = () => {
  return {
    type: 'SHOW_ALL'
  }
}