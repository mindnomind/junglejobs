import React from 'react'
import {connect} from 'react-redux'

import Label from './Label'

const getLabels = (state) => {
    let labelsOut = [],
    	news = state.news.data
    news.forEach((post)=>{
    	let labels = post.labels.filter((lable)=>{return labelsOut.indexOf(lable) == -1})
    	if (labels.length > 0){
    		labelsOut = labelsOut.concat(labels);
    	}
    })
    return labelsOut
}

const mapStateToProps = (state) => {
  return {
  	labels: getLabels(state)
  }
}

const CloudStyle = {
	root : {
		margin: '10px'
	}
}

class LabelsCloud extends React.Component {
	render(){
		return (
			<div style={CloudStyle.root}>
				{this.props.labels.map((label,i) =>{
					return(
						<Label key={i} name={label}/>
					)
				}
				)}
			</div>
		)
	}
}

const Out = connect(mapStateToProps)(LabelsCloud);

export default Out