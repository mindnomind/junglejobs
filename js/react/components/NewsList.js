import React from 'react'
import {connect} from 'react-redux'

import moment from 'moment'

const getVisibleNews = (state) => {
	let news = state.news.data,
		currentLabels = state.filter.currentLabels
	state.filter.types.forEach((type)=>{
		
		if (type == 'SET_DATE_FILTER'){
			news = news.filter((post)=>{
		    	return post.date == state.filter.date.format("YYYY-MM-DD")
		  	})
		}

		if (type == 'SET_LABEL_FILTER') {
        	let filteredNews = []
        	news.forEach(function(post){
          		let congruenceNum = 0
          		currentLabels.forEach(function(currentLabel){
            		if (post.labels.indexOf(currentLabel) != -1) congruenceNum++
            	})
          		if (congruenceNum == currentLabels.length) filteredNews.push(post);
        	})
        	news = filteredNews
      	 } 

	})
	return news
}

const mapStateToProps = (state) => {
  return {
  	news: getVisibleNews(state)
  }
}

const NewsListStyle = {
	root : {
		width: '100%',
		height: 'auto',
		padding: '20px 18px',
		backgroundColor: '#fff',
		boxShadow: 'inset 0 4px 0 #444',
		border: '1px solid #999',
		borderTop: 'none',
		marginBottom: '20px',
	},
	title : {
		fontFamily: '"PT Sans",sans-serif',
		fontSize: '34px',
		lineHeight: '36px',
		color: '#333'
	},
	text: {
    	fontFamily: '"Tinos",serif',
    	fontSize: '20px',
    	lineHeight: '25px',
    	color: '#333',
    	letterSpacing: '0.02em'
  	},
  	labels: {
  		marginBottom: '20px'
  	},
  	date: {
  		color: '#868181',
    	marginTop: '8px',
    	fontSize: '13px',
    	fontFamily: 'PT Sans',
    	fontWeight: 'bold'
	},
	label: {
		fontFamily: '"PT Sans",sans-serif',
    	padding: '5px 8px',
    	border: '2px solid #ccc',
   		width: 'auto',
    	color: '#868181',
    	textTransform: 'uppercase',
    	fontSize: '13px',
    	letterSpacing: '.04em',
      	marginRight: '15px',
      	display: 'inline-block'
    }
}

class NewsList extends React.Component {
	render(){
		return (
			<div>
			{this.props.news.map((post,i) => {
 				return(
 					<div key={i} style={NewsListStyle.root}>
 						<h2 style={NewsListStyle.title}>{post.title}</h2>
 						<div>
 							<div style={NewsListStyle.labels}>
 								{post.labels.map((label,j)=>{
 									return (
 										<div key={j} style={NewsListStyle.label}>{label}</div>
 									)
 								})}
 							</div>
 						</div>
 						<div style={NewsListStyle.text}>{post.text}</div>
 						<div style={NewsListStyle.date}>{post.date}</div>
 					</div>
 				)
			})}
			</div>
		)
	}
}

const Out = connect(mapStateToProps)(NewsList);

export default Out