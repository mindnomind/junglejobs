import React from 'react'
import {connect} from 'react-redux'

import {setLabelFilter} from '../actions'

const mapStateToProps = (state,ownProps) => {
  return {
    color: state.filter.currentLabels.indexOf(ownProps.name) == -1 ? '#868181' : '#fff'
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setLabel: (label) => {
      dispatch(setLabelFilter(label))
    }
  }
}


	

class Label extends React.Component {
  render(){

    const labelStyle = {
      fontFamily: '"PT Sans",sans-serif',
      padding: '5px 8px',
      border: '2px solid #ccc',
      width: 'auto',
      color: this.props.color,
      textTransform: 'uppercase',
      fontSize: '13px',
      letterSpacing: '.04em',
      cursor: 'pointer'
    }

		return (
			<div style={labelStyle} onClick={()=>{this.props.setLabel(this.props.name)}}>{this.props.name}</div>
		)
	}
}

const Out = connect(mapStateToProps,mapDispatchToProps)(Label);

export default Out