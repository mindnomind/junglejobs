import React from 'react'
import {connect} from 'react-redux'

import NewsList from './NewsList'
import LabelsCloud from './LabelsCloud'
import DatePicker from 'react-datepicker'

import {changeDate,showAll} from '../actions'

import moment from 'moment'

const mapStateToProps = (state) => {
  return {
  	currentDate: state.filter.date
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    changeDate: (date) => {
    	dispatch(changeDate(date))
    },
    showAll: () => {
      dispatch(showAll())
    }
  }
}

const AppStyle = {
  root: {
    width: '100%',
    height: '100%'
  },
  wrapper: {
    width: '960px',
    height: '100%',
    margin: '0 auto',
  },
  sidebar: {
    width: '256px',
    height: '100%',
    float: 'left',
    in: {
      padding: '43px 0 0 0'
    }
  },
  main: {
    width: '704px',
    float: 'right',
    height: '100%'
  },
  title: {
    fontSize: '50px',
    color: '#fff',
    textShadow: 'rgb(68, 68, 68) 3px 2px 2px',
    fontFamily: '"PT Sans", sans-serif',
    letterSpacing: '0.04em'
  },
  showAll: {
    fontFamily: '"PT Sans",sans-serif',
    color: '#333',
    textDecoration: 'underline',
    cursor: 'pointer'
  }
}

class NewsFilterApp extends React.Component {
  render(){
  	return(
    	<div style={AppStyle.root}>
    		<div style={AppStyle.wrapper}>
          <div style={AppStyle.sidebar}>
            <div style={AppStyle.sidebar.in}>
        			<DatePicker
                locale="ru_ru" 
        				selected={this.props.currentDate} 
        				onChange={this.props.changeDate} 
        				maxDate={moment()}
        			/>
              <LabelsCloud />
              <span style={AppStyle.showAll} onClick={()=>{this.props.showAll()}}>Показать все</span>
            </div>
      		</div>
      		<div style={AppStyle.main}>
      			<h1 style={AppStyle.title}>Новости</h1>
      			<NewsList />
      		</div>
        </div>
    	</div>
    )  
  }
}

const Out = connect(mapStateToProps,mapDispatchToProps)(NewsFilterApp);

export default Out
