import _ from 'underscore'

const filter = (state = {}, action) => {
	switch(action.type){

		case 'SHOW_ALL':
			return {...state,
				currentLabels: [],
				types: []
			}

		case 'CHANGE_DATE':
			return {...state,
				date: action.date,
				types: state.types.indexOf('SET_DATE_FILTER') == -1 ? state.types.concat('SET_DATE_FILTER') : state.types
			}

		case 'SET_LABEL_FILTER':
			return {...state,
				currentLabels: state.currentLabels.indexOf(action.label) == -1 ? state.currentLabels.concat(action.label) : _.without(state.currentLabels,action.label),
				types: state.types.indexOf('SET_LABEL_FILTER') == -1 ? state.types.concat('SET_LABEL_FILTER') : state.types
			}

		default:
			return state;
	}
}

export default filter