import 'babel-polyfill'

import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, combineReducers } from 'redux'

import news from './reducers/news'
import filter from './reducers/filter'

import NewsFilterApp from './components/NewsFilterApp'

import moment from 'moment'

const reducer = combineReducers({
  news,
  filter
})

const initialState = {
	news: {
		data: [
			{
				labels: ['Собаки','Искусство'],
				title: 'Оставшись один на три часа, хаски превратил квартиру в «арт-объект»',
				text: 'Как объяснил знакомый хозяев, представивший работу общественнности, хаски использовал «традиционные чернила для китайской каллиграфии», 100% биологически чистые и нетоксичные для людей и собак.',
				date: '2016-01-01'
			},
			{
				labels: ['Обезьяны','Электричество'], 
				title: 'Мартышка обесточила всю Кению на три часа', 
				text: 'Жители Кении на три часа остались без электричества из-за обезьяны. Как сообщает энергетическая компания  KenGen в своем фейсбуке, карликовая зеленая мартышка успешно пробралась через забор, находящийся под напряжением, забралась на крышу гидроэлектростанции «Джитару» и упала на трансформатор. Его отключение привело «к перегрузке других устройств на ГЭС и потере более 180 МВт на этой станции, что стало причиной общенационального блэкаута»', 
				date: '1995-12-25'
			},
			{
				labels: ['Обезьяны','Кража'],
				title: 'В Китае обезьяна дерзко похитила мобильный телефон туристки и скрылась в неизвестном направлении',
				text: 'В горах Эмэйшань китайской провинции Сычуань обезьяна украла телефон у туристки, снимавшей животное на камеру мобильного, сообщает Daily Mail.',
				date: '2014-10-01'
			}
		]
	},
	filter: {
		date: moment(),
		currentLabels: [],
		types: []
	}
}

const store = createStore(reducer,initialState)



const mainRender = () => {
		console.log('State');
		console.log('--------------');
		console.log(store.getState());
		render(
			<Provider store={store}>
				<NewsFilterApp />
			</Provider>
			,
			document.getElementById('root')
		)
		
	}

store.subscribe(mainRender)
mainRender()