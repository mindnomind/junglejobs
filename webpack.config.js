var webpack = require('webpack'),
    ExtractTextPlugin = require('extract-text-webpack-plugin');
    

module.exports = {
       context: __dirname,
       entry: {
        app: ['./js/include.js'],
       },
       output: {
          path: "./js/",
          filename: "bundle.js"
       },
       devtool: 'source-map',
       module: {
         loaders: [
           {
            loader: "babel-loader",
            exclude: /node_modules/,
            test: /\.js?$/,
            query: {
              plugins: ['transform-runtime'],
              presets: ['es2015', 'stage-0', 'react'],
            }
           },
           {
             test: /\.css$/,
             loader: ExtractTextPlugin.extract("style-loader", "css-loader") 
           },
           {
             test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
             loader: 'url-loader?limit=10000&mimetype=application/font-woff&name=../css/[hash].[ext]'
           },
           {
             test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
             loader: 'file-loader?name=../css/[hash].[ext]'
           }
        ]
       },
       plugins: [
        new ExtractTextPlugin("../css/bundle.css")
       ],
};
